#***********************************************************************************************************************
# File Name       : pic24_uploader.py
# Description     :
# Original Author : Chao Wang
# Created on      : Sep. 22, 2021, 12:42 PM
#***********************************************************************************************************************
from __future__ import print_function
import sys

try:
    import argparse
    from intelhex import IntelHex
    import os
    import serial
    import time  
except ImportError:
    sys.exit("""ImportError: You are probably missing some modules.
To add needed modules, run like 'python -m pip install -U future pyserial intelhex'""")

#-----------------------------------------------------------------------------------------------------------------------
# Generate help and use messages
parser = argparse.ArgumentParser(
    description='Serial bootloader script for Microchip PIC24 family MCUs',
    epilog='Example: pic24_uploader.py ./App/Release/App.hex 0x2000 0x57800 COM5 9600',
    formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('file', help='Hex file will be uploaded')
parser.add_argument('application_start', help='Application start address based on words is equal to application_start')
parser.add_argument('application_end', help='Application end address based on words is equal to (application_end - 1)')
parser.add_argument('comport', help='UART COM port')
parser.add_argument('baudrate', help='UART baud rate')

if len(sys.argv) != 6:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

# Command line arguments
File = sys.argv[1]
AppStartAddrInWords = int(sys.argv[2], 16)
AppEndAddrInWords = int(sys.argv[3], 16)
ComPort = sys.argv[4]
Baudrate = int(sys.argv[5], 10)

#-----------------------------------------------------------------------------------------------------------------------
# Variables
UART = None             # flag for UART open

GoBuf = bytearray()     # global output buffer for functions OutPacket/InCom
RcvBuf = bytearray()    # global input buffer for functions OutPacket/InCom
FBuf = bytearray()

EraseSizeW = 0x20       # erase row size (words), will be update during get version process.
WriteSizeW = 0x20       # Write latches per row size (words), will be update during get version process.

# The following three items will be update during get version process.
MaxPacketSizeInBytes = 0x100    # Explain in protocol
ErasePaseSizeInWords = 0x800    # One page is equal to 8 rows or 1024 instructions, one instruction is 2 bytes.
MinWriteSizeInBytes = 0x8       # Program one double instruction word or one row at a time.

ProgramSizeInBytesEveryTime = 0x80  # This add necessary cmd should be less than MaxPacketSizeInBytes.

AppStartAddrInBytes = AppStartAddrInWords<<1
AppEndAddrInBytes = AppEndAddrInWords<<1

LENGTH_RESPONSE_VERSION = 37
LENGTH_RESPONSE_ERASE = 12
LENGTH_RESPONSE_WRITE = 12
LENGTH_RESPONSE_VERIFY = 12
LENGTH_RESPONSE_RESET = 12


#***********************************************************************************************************************
# Function : hex2bin(hex_file, flash_start, flash_end)
#***********************************************************************************************************************
def hex2bin(hex_file, flash_start, flash_end):
    # Load application hex file and convert to bin file
    ih = IntelHex()
    fileextension = hex_file[-3:]
    ih.loadfile(hex_file, format=fileextension)

    app_bin = ih.tobinarray(start=flash_start, end=(flash_end - 1))

    bin_file = os.path.splitext(hex_file)[0] + ".bin"

    # Save original file
    fq = open(bin_file, 'wb')
    fq.write(app_bin)
    fq.close()

#***********************************************************************************************************************
# Function : out_packet()
#***********************************************************************************************************************
def out_packet():  # General Command Format
    global GoBuf

    UART.write(GoBuf)

#***********************************************************************************************************************
# Function : in_com(timeout)
#***********************************************************************************************************************
def in_com(timeout, rcvlen):  # timeout == 0 ==> wait until got data
    global RcvBuf

    RcvBuf = bytearray(b'')
    tStart = time.perf_counter()
    Retry = 3
    Length = 0

    while True: 
        
        bdata = UART.read()
        
        if len(bdata) > 0:
            Length += len(bdata)
            RcvBuf.extend(bdata)
            if Length == rcvlen:
                return 1
        elif timeout != 0:
            if (time.perf_counter() - tStart) > timeout:  # timeout loop & retry
                if Retry == 0: return 0
                print(
                    "Status: No response in " + str(timeout) + " S," + " re-trying " + str(Retry) + ' more time(s).')
                Retry -= 1
                #UART.write(GoBuf)
                tStart = time.perf_counter()

#***********************************************************************************************************************
# Function : execute_result(TOut)
#***********************************************************************************************************************
def execute_result(TOut, RcvLength):
    global GoBuf

    out_packet()

    if in_com(TOut, RcvLength) == 0:
        print("No response error, Process terminated !")
        return False

    if ((GoBuf[0] == 0x02) or (GoBuf[0] == 0x03)) and RcvBuf[11] != 1:
        if RcvBuf[11] == 0xFE:
            print("ADDRESS OUT OF RANGE ERROR when executing command %0X !!" % GoBuf[0])
        elif RcvBuf[11] == 0xFF:
            print("Invalid Command ERROR when executing command %0X !!" % GoBuf[0])            
        else:
            print("Unknown ERROR when executing command %0X !!" % GoBuf[0])
        return False  

    return True

#***********************************************************************************************************************
# Function : open_uart()
#***********************************************************************************************************************
def open_uart():
    global UART

    if UART == None:
        try:
            UART = serial.Serial(ComPort, baudrate=Baudrate, timeout=0.01) # timeout determin speed of update process.
            UART.reset_input_buffer()
            UART.reset_output_buffer()
        except:
            print('Status: ' + 'open ' + ComPort + ' fail!!')
            return False
    return True
    
#***********************************************************************************************************************
# Function : close_uart()
#***********************************************************************************************************************
def close_uart():
    global UART

    if UART != None:
        try:
            UART.close()
        except:
            print('Status: ' + 'close ' + ComPort + ' fail!!')
            return False
    return True    

#***********************************************************************************************************************
# Function : get_version()
#***********************************************************************************************************************
def get_version():
    global GoBuf, MaxPacketSizeInBytes, ErasePaseSizeInWords, MinWriteSizeInBytes
    global ProgramSizeInBytesEveryTime

    print("*******************Read Version Command...*******************\n")
    print("Hint: Getting version ...\n")

    GoBuf = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')

    print("Tx ->", GoBuf.hex(' '), "\n")

    if execute_result(1.0, LENGTH_RESPONSE_VERSION) == False: sys.exit(1)
    print("Rx ->", RcvBuf[0:].hex(' '), "\n")

    MaxPacketSizeInBytes = RcvBuf[13] + (RcvBuf[14]<<8)
    ErasePaseSizeInWords = RcvBuf[21] + (RcvBuf[22]<<8)
    MinWriteSizeInBytes = RcvBuf[23] + (RcvBuf[24]<<8)

    MaxPacketSizeInBytes = 0x100
    ErasePaseSizeInWords = 0x800
    MinWriteSizeInBytes = 0x8

    ProgramSizeInBytesEveryTime = ((MaxPacketSizeInBytes - 11)//MinWriteSizeInBytes) * MinWriteSizeInBytes

    # Make ProgramSizeInBytesEveryTime could be divied by row 512 bytes(128 instructions).
    # Actually it is always smaller than MaxPacketSizeInBytes.
    if ProgramSizeInBytesEveryTime > 512:
        ProgramSizeInBytesEveryTime = 512
    elif ProgramSizeInBytesEveryTime > 256:
        ProgramSizeInBytesEveryTime = 256
    elif ProgramSizeInBytesEveryTime > 128:
        ProgramSizeInBytesEveryTime = 128
    elif ProgramSizeInBytesEveryTime > 64:
        ProgramSizeInBytesEveryTime = 64
    elif ProgramSizeInBytesEveryTime > 32:
        ProgramSizeInBytesEveryTime = 32
    elif ProgramSizeInBytesEveryTime > 16:
        ProgramSizeInBytesEveryTime = 16
    elif ProgramSizeInBytesEveryTime > 8:
        ProgramSizeInBytesEveryTime = 8    

    print("Status: Get version completely", 'successful!\n')

#***********************************************************************************************************************
# Function : erase_flash(MinAddr, MaxAddr, RowSize)
#***********************************************************************************************************************
def erase_flash(MinAddr, MaxAddr, PageSize):
    global GoBuf

    print("*******************Erase Flash Command...********************\n")
    print("Hint: Erasing flash ...\n")
    
    EraseCnt = int((MaxAddr - MinAddr) / PageSize)

    GoBuf = bytearray(b'\x03') + EraseCnt.to_bytes(2, byteorder='little') + bytearray(b'\x55\x00\xAA\x00') + \
                MinAddr.to_bytes(4, byteorder='little')
    print("Tx ->", GoBuf.hex(' '), "\n")                

    if execute_result(10.0, LENGTH_RESPONSE_ERASE) == False: sys.exit(1)   #If time is out, then will resend, but software of MCU side doesn't support that.
    print("Rx ->", RcvBuf[0:].hex(' '), "\n")

    print("Status: Erase flash memory", 'successful!\n')   

#***********************************************************************************************************************
# Function : write_flash(MinAddr, MaxAddr, RowSize)
#***********************************************************************************************************************
def write_flash(MinAddr, MaxAddr, ProgramSizeEveryTime):
    global FBuf, File, GoBuf

    print("*******************Write Flash Command...********************\n")
    print("Hint: Writing flash ...\n")

    bin_file = os.path.splitext(File)[0] + ".bin"
    #size = os.path.getsize(bin_file);
    #print("Uploading", size, "bytes from bin file...\n")

    with open(bin_file, "rb") as f:
        FBuf += f.read()

    EmptyArray = bytearray(b'\xff' * ProgramSizeEveryTime)

    for Address in range(MaxAddr - ProgramSizeEveryTime, MinAddr - ProgramSizeEveryTime, -ProgramSizeEveryTime):

        GoBuf = bytearray(b'\x02') + ProgramSizeEveryTime.to_bytes(2, byteorder='little') + bytearray(b'\x55\x00\xAA\x00') + \
                    (Address>>1).to_bytes(4, byteorder='little')

        if EmptyArray == FBuf[Address - MinAddr: Address - MinAddr + ProgramSizeEveryTime]:
            continue

        GoBuf += FBuf[Address - MinAddr: Address - MinAddr + ProgramSizeEveryTime]

        print("-------------------------------------------------------------\n")
        print("Programming range from 0X%08XH to 0X%08XH. (Whole range is from 0X%08Xh to 0X%08Xh)"
              % ((Address>>1), (((Address + ProgramSizeEveryTime)>>1) - 1), (MinAddr>>1), ((MaxAddr>>1) - 1)), '...\n')
        print("Tx ->", GoBuf.hex(' '), "\n") 

        if execute_result(10.0, LENGTH_RESPONSE_WRITE) == False:
            sys.exit(1)
        print("Rx ->", RcvBuf[0:].hex(' '), "\n")

    print("Status: Writing flash memory successfully !!  Range from 0X%08Xh to 0X%08Xh.\n" % ((MinAddr>>1), ((MaxAddr>>1) - 1)))
      
#***********************************************************************************************************************
# Function : self_verify()
#***********************************************************************************************************************
def seff_verify():
    global GoBuf
 
    print("*******************Self Verify Command...********************\n")
    print("Hint: Self verify ...\n")

    GoBuf = bytearray(b'\x0A\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
    print("Tx ->", GoBuf.hex(' '), "\n") 

    if execute_result(10.0, LENGTH_RESPONSE_VERIFY) == False: sys.exit(1)
    print("Rx ->", RcvBuf[0:].hex(' '), "\n")

    if RcvBuf[11] != True:
        print("Status: Self verify fail!\n")
        sys.exit(1)
    else:
        print("Status: Self verify successful!\n")

#***********************************************************************************************************************
# Function : reset_device()
#***********************************************************************************************************************
def reset_device():
    global GoBuf

    print("*******************Reset Device Command...*******************\n")
    print("Hint: reset device ...\n")

    GoBuf = bytearray(b'\x09\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
    print("Tx ->", GoBuf.hex(' '), "\n") 
    
    out_packet()    
    print("Status: Reset device successful!\n")  

#***********************************************************************************************************************
# Function : main
#***********************************************************************************************************************
if __name__ == "__main__":
    os.system('')
    print("\033[1;34;40m")
    if open_uart() == True:
        print("**********************BOOTLOAD START*************************\n")    
        hex2bin(File, AppStartAddrInBytes, AppEndAddrInBytes)
        get_version()        
        erase_flash(AppStartAddrInWords, AppEndAddrInWords, ErasePaseSizeInWords)
        write_flash(AppStartAddrInBytes, AppEndAddrInBytes, ProgramSizeInBytesEveryTime)
        seff_verify()
        reset_device()
        close_uart()
        print("*********************BOOTLOAD COMPLETE***********************\n")     
        sys.exit(1)
    else:
        sys.exit(1)
    print("\033[0m") 